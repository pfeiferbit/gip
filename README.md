Grundlagen der Informatik und höhere Programmiersprachen
========================================================

Die hier veröffentlichen Lösungen können vielleicht jemandem beim Finden von
eigenen Lösungen helfen, sind aber nicht als generelle Kopierpaste gedacht.

Das wichtigste beim GIP-Praktikum ist, dass man die grundlegenden Konzepte
versteht. Der Code ist lediglich Nebensache.

Fragen oder Kritik bitte persönlich auf Facebook oder hier per eMail.

Falls nicht anders möglich (z.B. weil GPL-lizenzierte Bibliotheken verwendet
wurden) oder vermerkt, wird alles in diesem Repository unter der MIT-Lizenz
veröffentlicht.

Copyright (c) 2013 Maximilian Pfeifer
