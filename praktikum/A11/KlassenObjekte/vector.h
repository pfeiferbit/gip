/**
 * @file   vector.h
 * @brief  A11 - Klassen und Objekte
 * @author Maximilian Pfeifer
 */

class Vector
{

private:
    int* data;
    int  size;
    int  id;
    static int counter;

public:
    Vector();
    Vector(int size);
    Vector(Vector &);
    ~Vector();

    void set(int data[], int size); 
    void print();
    Vector operator+(Vector &obj);

};
