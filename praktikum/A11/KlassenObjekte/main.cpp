/**
 * @file   main.cpp
 * @brief  A11 - Klassen und Objekte
 * @author Maximilian Pfeifer
 */

#include <iostream>

#include "vector.h"

int main() {
    Vector v1(3),
           v2(3),
           v3(2);
    Vector vSum(v1 + v2),
           vErr(v1 + v3);
    //vSum.print();
    //vErr.print();
    system("PAUSE");
    return 0;
}