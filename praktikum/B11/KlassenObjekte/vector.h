/**
 * @file   vector.h
 * @brief  B11 - Klassen und Objekte (aus A11)
 * @author Maximilian Pfeifer
 */

class Vector
{

private:
    int* data;
    int  size;
    int  id;
    static int counter;

public:
    Vector();
    Vector(int size);
    Vector(Vector &obj);
    ~Vector();

    void set(int data[], int size); 
    void print();
    Vector operator+(Vector &obj);

};
