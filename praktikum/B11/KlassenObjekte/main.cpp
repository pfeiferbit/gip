/**
 * @file   main.cpp
 * @brief  B11 - Klassen und Objekte
 * @author Maximilian Pfeifer
 */

#include <iostream>

#include "vector.h"

using namespace std;

int main()
{
    cout << "--- Vektor A(2);\n";
    Vector A(2);
    cout << "\n--- A.DisplayVektor();\n";
    A.print();
    cout << "\n--- Vektor B=A;\n";
    Vector B=A;
    cout << "--- A + B;\n";
    A + B;
    cout << "\n--- Vektor C=A+B;\n";
    Vector C=A+B;
    cout << "\n--- C.DisplayVektor();\n";
    C.print();
    cout << "\n--- return 0;\n";
    system("PAUSE");
    return 0;
}