/**
 * @file   vector.cpp
 * @brief  B11 - Klassen und Objekte (aus A11)
 * @author Maximilian Pfeifer
 */

#include "vector.h"

#include <iostream>

int Vector::counter = 0;

Vector::Vector() : size(0)
{
    counter++;
    id   = counter;
    data = nullptr;
}

Vector::Vector(int size) : size(size)
{
    counter++;
    id = counter;
    if (size > 0) {
        data = new int[size];
        for (int i = 0; i < size; i++) {
            int zahl;
            std::cout << i+1 << ". Vektorelement = ? ";
            std::cin  >> zahl;
            data[i] = zahl;
        }
    } else {
        data = nullptr;
    }
}

Vector::Vector(Vector &obj)
{
    counter++;
    id   = counter;
    size = obj.size;
    if (obj.data) {
        data = new int[obj.size];
        memcpy(data, obj.data, sizeof(int)*size);
    } else {
        data = nullptr;
    }
    std::cout << "Kopierkonstruktor:" << std::endl;
    print(); 
    obj.print();
    std::cout << std::endl;
}

Vector::~Vector()
{
    std::cout << "Destruktor: ";
    print();
    std::cout << std::endl;
    delete []data;
}

void Vector::set(int data[], int size)
{
    if (data && size > 0) {
        delete []this->data;
        this->data = new int[size];
        this->size = size;
        memcpy(this->data, data, sizeof(int)*size);
    }
}

void Vector::print()
{
    if (data) {
        std::cout << "Vektor "<< id << ": " << data <<" {";
        for (int i = 0;  i < (size-1); i++)
            std::cout << data[i] << " ";
        std::cout << data[size-1] << "}";
    }
    std::cout << std::endl;
}

Vector Vector::operator+(Vector &obj)
{
    int* sums = nullptr;
    if (data && obj.data && size > 0 && obj.size == size) {
        sums = new int[size];
        for (int i = 0; i < size; i++) {
            sums[i] = data[i] + obj.data[i];
        }
    }
    Vector result;
    result.set(sums, size);
    delete[] sums;
    std::cout << "Vektor Temp (in operator +): "; result.print();
    std::cout << "Vektor OP1  (in operator +): "; print();
    std::cout << "Vektor OP2  (in operator +): "; obj.print();
    return result;
}
