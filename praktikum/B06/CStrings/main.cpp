/**
 * @file main.cpp
 * @brief B06 C-Strings
 * @author Maximilian Pfeifer
 * @date 2013-11-14
 */

#include <iostream>

using namespace std;

const int len = 80;

/* Bestimmung der Laenge einer Variable mittels hochzahlendem Index */
unsigned int mystrlen(char const* string) {
    int i = 0;
    while (string[i] != 0)
        i++;
    return i;
}

/* Konkatenierung von 2 C-Strings durch Kopie in eine neue Zeichenkette */
char* mystrnconcat(char const* first, char const* second, unsigned int count) {
    const int catlen = (len-1) * 2 + 1;
    char* cat = new char[catlen];
    memset(cat, 0, catlen);
    unsigned int i = 0;
    for (int k = 0; first[k] != 0 && i < count; i++, k++)
        cat[i] = first[k];
    for (int l = 0; second[l] != 0 && i < count; i++, l++)
        cat[i] = second[l];
    return cat;
}

/* Finden eines Teilstrings und dann die Adresse des ersten Zeichens geben */
char* mystrfind(char* first, char* second) {
    for (int i = 0; first[i] != 0; i++) {
        int k = 0;
        for (int j = i; first[j] == second[k] && second[k] != 0; j++, k++);
        if (second[k] == 0)
            return &first[i];
    }
    return nullptr;
}

/* Einsprungspunkt */
int main() {
    char first[len] = {0},
         second[len] = {0};
    int selection, count = 0;
    cout << "Bitte waehlen Sie die Operation aus." << endl
        << "-1- mystrlen()" << endl
        << "-2- mystrnconcat()" << endl
        << "-3- mystrfind()" << endl;
    cin >> selection;
    cin.clear();
    cin.ignore(1000, '\n');
    if (selection == 1) {
        cout << "Zeichenkette? ";
        cin.getline(first, len);
        cout << "Ergebnis mystrlen(): " << mystrlen(first) << endl;
    } else if (selection == 2) {
        cout << "Erste Zeichenkette? ";
        cin.getline(first, len);
        cout << "Zweite Zeichenkette? ";
        cin.getline(second, len);
        cout << "Anzahl Zeichen? ";
        cin >> count;
        cout << "Ergebnis mystrnconcat(): "
             << mystrnconcat(first, second, count) << endl;
    } else if (selection == 3) {
        cout << "Erste Zeichenkette? ";
        cin.getline(first, len);
        cout << "Zweite Zeichenkette? ";
        cin.getline(second, len);
        cout << "Ergebnis mystrfind(): ";
        char* found = mystrfind(first, second);
        if (found)
            cout << found;
        else
            cout << "Nicht gefunden.";
        cout << endl;
    }
    system("PAUSE");
    return 0;
}
