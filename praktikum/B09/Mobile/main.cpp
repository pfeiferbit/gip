/**
 * @file main.cpp
 * @brief B09 - Mobile als Klasse
 * @author Maximilian Pfeifer
 */

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

class Mobile {

private:
    size_t id;
    string brand;
    string model;
    double price;
    double mileage;
    string fuel;
    string zip;

    Mobile();

    inline static string nice_label(string name) {
        return name + string(15-name.length(), ' ');
    }

public:
    Mobile(size_t id) : id(id) {}

    void input() {
        stringstream instream;
        string input = "";
        auto in = [&input] (char* name) {
            cout << nice_label(string(name)) << "= ? ";
            getline(cin, input);
            stringstream instream(input);
            return instream;
        };
        instream = in("Marke");          instream >> brand;
        instream = in("Modell");         instream >> model;
        instream = in("Preis");          instream >> price;
        instream = in("Kilometerstand"); instream >> mileage;
        instream = in("Kraftstoffart");  instream >> fuel;
        instream = in("PLZ");            instream >> zip;
    }

    void printout() {
        ostringstream outstream;
        auto out = [&outstream] (char* name) {
            cout << nice_label(string(name)) << "=  "
                 << outstream.str() << endl;
            outstream.str("");
        };
        outstream << brand;   out("Marke");
        outstream << model;   out("Modell");
        outstream << price;   out("Preis");
        outstream << mileage; out("Kilometerstand");
        outstream << fuel;    out("Kraftstoffart");
        outstream << zip;     out("PLZ");
    }

    bool search(Mobile &m) {
        if (m.brand   != string("0") && m.brand   == brand   ||
            m.model   != string("0") && m.model   == model   ||
            m.price   != double(0)   && m.price   >= price   ||
            m.mileage != double(0)   && m.mileage >= mileage ||
            m.fuel    != string("0") && m.fuel    == fuel    ||
            m.zip     != string("0") && m.zip     == zip)
            return true;
        else
            return false;
    }

};

int main() {
    const size_t N = 1;
    vector<Mobile> mobiles;
    for (size_t i = 0; i < N; i++) {
        mobiles.push_back(Mobile(i));
        mobiles[i].input();
        cout << endl;
    }
    cout << endl << "Wonach suchen Sie?" << endl;
    Mobile compare(0);
    compare.input();
    for (auto it = mobiles.begin(); it != mobiles.end(); ++it) {
        if ((*it).search(compare)) {
            cout << endl;
            (*it).printout();
        }
    }
    cout << endl;
    system("PAUSE");
    return 0;
}