/**
 * @file main.cpp
 * @brief A06 - Doppelt verkettete Liste
 * @author Maximilian Pfeifer
 * @date 2013-11-14
 */

#include <iostream>

using namespace std;

/* Listenknoten fuer doppelte Verkettung (2 Pointer) */
struct node {
    int   data;
    node* next;
    node* prev;
};

/* Hinten anfuegen, O(n) */
void push_back(node* &root, int val) {
    node* current = new node;
    current->data = val;
    current->next = nullptr;
    if (root == nullptr) {
        current->prev = nullptr;
        root = current;
    } else {
        node* tmp = root;
        for (tmp; tmp->next != nullptr; tmp = tmp->next);
        current->prev = tmp;
        tmp->next = current;
    }
}

/* Liste ausgeben, O(n) */
void print_list(node* root) {
    if (root == nullptr)
        cout << "Leere Liste.";
    else {
        cout << "[ " << root->data;
        for (node* tmp = root; tmp->next != nullptr; tmp = tmp->next)
            cout << ", " << tmp->next->data;
        cout << " ]";
    }
    cout << endl;
}

/* Liste von hinten ausgeben, O(2n) */
void print_list_back(node* root) {
    if (root == nullptr)
        cout << "Leere Liste.";
    else {
        node* tmp = root;
        for (tmp; tmp->next != nullptr; tmp = tmp->next);
        cout << "[ " << tmp->data;
        for (tmp; tmp->prev != nullptr; tmp = tmp->prev)
            cout << ", " << tmp->prev->data;
        cout << " ]";
    }
    cout << endl;
}

/* In Liste einfuegen, O(n) */
void insert(node* &root, int val, int successor) {
    node* current = new node;
    current->data = val;
    if (root == nullptr) {                // 1. Fall: Root setzen
        current->next = nullptr;
        current->prev = nullptr;
        root = current;
    } else if (root->data == successor) { // 2. Fall: Root wird Nachfolger
        current->next = root;
        current->prev = nullptr;
        current->next->prev = current;
        root = current;
    } else {                              // 3. Fall: Element X wird Nachfolger
        node* tmp = root;
        for (tmp; tmp->next != nullptr; tmp = tmp->next)
            if (tmp->next->data == successor)
                break;
        current->next = tmp->next;
        current->prev = tmp;
        current->prev->next = current;
        if (current->next != nullptr)
            current->next->prev = current;
    }
}

/* Einsprungspunkt */
int main() {
    const int len = 10;
    node* root = nullptr;
    //print_list(root);
    for (int i = 0; i < len; i++)
        push_back(root, i*i);
    print_list(root);
    //print_list_back(root);
    int val, successor;
    cout << "Einzufuegender Wert: ";
    cin >> val;
    cout << "Vor welchem Wert? ";
    cin >> successor;
    insert(root, val, successor);
    print_list(root);
    system("PAUSE");
    return 0;
}
