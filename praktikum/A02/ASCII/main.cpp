/**
 * @file main.cpp
 * @brief GIP Praktikum A02: ASCII
 * @author Maximilian Pfeifer
 * @date 2013-10-15
 */

#include <iostream>

using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {

	// A02.01
	char
		c1 = '\n',
		c2 = '\t',
		c3 = '\a',
		c4 = '\"',
		c5 = '\'',
		c6 = 'a',
		c7 = 'Z';
	cout
		<< "Newline: " << c1
		<< "Tab: " << c2 << "Bell: " << c3 << endl
		<< "Doppeltes Hochkomma: " << c4 << endl
		<< "Einfaches Hochkomma: " << c5 << endl
		<< "Buchstabe a: " << c6 << endl
		<< "Buchstabe Z: " << c7 << endl
		<< endl;

	// A02.02
	cout
		<< "Der Wert von Newline nach int konvertiert lautet: " << int(c1) << endl
		<< "Der Wert von Tab nach int konvertiert lautet: " << int(c2) << endl
		<< "Der Wert von Bell nach int konvertiert lautet: " << int(c3) << endl
		<< "Der Wert des doppelten Hochkomma nach int konvertiert lautet: " << int(c4) << endl
		<< "Der Wert des einfachen Hochkomma nach int konvertiert lautet: " << int(c5) << endl
		<< "Der Wert des Buchstaben a nach int konvertiert lautet: " << int(c6) << endl
		<< "Der Wert des Buchstaben Z nach int konvertiert lautet: " << int(c7) << endl;

	system("PAUSE");
	return 0;
}