/**
 * @file main.cpp
 * @brief GIP Praktikum A02: Rechner ohne Uebertrag
 * @author Maximilian Pfeifer
 * @date 2013-10-15
 */

#include <iostream>
using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {

	// A02.03
	char a1, a2, b1, b2;

	cout << "Bitte geben Sie die Zehnerziffer der ersten Zahl ein: ";
	cin >> a1;
	cout << "Bitte geben Sie die Einerziffer der ersten Zahl ein: ";
	cin >> a2;
	cout << "Bitte geben Sie die Zehnerziffer der zweiten Zahl ein: ";
	cin >> b1;
	cout << "Bitte geben Sie die Einerziffer der zweiten Zahl ein: ";
	cin >> b2;

	cout
		<< "Die Summe der beiden Zahlen "
		<< a1 << a2 << " + " << b1 << b2 << " lautet: ";

	char
		sum1 = a1+b1-'0',
		sum2 = a2+b2-'0';

	cout << int(sum1-'0') << int(sum2-'0') << endl;

	system("PAUSE");
	return 0;
}