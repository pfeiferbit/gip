/**
 * @file   main.cpp
 * @brief  B12 - Dateioperationen (Zahlen)
 * @author Maximilian Pfeifer
 */

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

size_t* write_files(char* fn_txt, char* fn_bin, const size_t rows) {
    ofstream of_txt(fn_txt);
    ofstream of_bin(fn_bin, ios::binary);
    size_t* sizes = new size_t[rows];
    if (of_txt.is_open() && of_bin.is_open()) {

        for (size_t row = 0; row < rows; row++) {
            double number;
            cout << "Zahl " << row+1 << " ? ";
            cin >> number;
            of_txt << number << endl;
            stringstream converter;
            converter << number;
            sizes[row] = converter.str().size();
            const char* numstr = converter.str().c_str();
            of_bin.write(numstr, sizes[row]);
        }
    } else {
        if (!of_txt.is_open()) {
            cout << "Fehler beim Erstellen bzw. Oeffnen der Datei "
                 << fn_txt << "!" << endl;
        }
        if (!of_bin.is_open()) {
            cout << "Fehler beim Erstellen bzw. Oeffnen der Datei "
                 << fn_bin << "!" << endl;
        }
    }
    of_txt.close();
    of_bin.close();
    return sizes;
}

void read_files(char* fn_txt, char* fn_bin, size_t* sizes, const size_t rows) {
    ifstream if_txt(fn_txt);
    ifstream if_bin(fn_bin, ios::binary);
    if (!if_txt.is_open()) {
        cout << "Fehler beim Oeffnen der Datei " << fn_txt << "!" << endl;
    } else {
        cout << "Zahlen gelesen aus Datei " << fn_txt << ":" << endl;
        for (size_t row = 0; row < rows && !if_txt.eof(); row++) {
            char* output = new char[sizes[row]+1];
            if_txt >> output;
            cout << output << endl;
            delete output;
        }
        cout << endl;
    }
    if (!if_bin.is_open()) {
        cout << "Fehler beim Oeffnen der Datei " << fn_bin << "!" << endl;
    } else {
        cout << "Zahlen gelesen aus Datei " << fn_bin << ":" << endl;
        for (size_t row = 0; row < rows && !if_bin.eof(); row++) {
            char* output = new char[sizes[row]+1];
            if_bin.read(output, sizes[row]);
            cout << *output << endl;
            delete output;
        }
        cout << endl;
    }
    if_txt.close();
    if_bin.close();
}

int main() {
    const size_t rows = 2;
    char* fn_txt = "Zahlen.txt";
    char* fn_bin = "Zahlen.bin";
    size_t* sizes = write_files(fn_txt, fn_bin, rows);
    cout << endl;
    read_files(fn_txt, fn_bin, sizes, rows);
    delete sizes;
    system("PAUSE");
    return 0;
}