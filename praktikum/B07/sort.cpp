/**
 * @file sort.cpp
 * @brief B07 - Makefile
 * @author Maximilian Pfeifer
 */

#include "sort.h"

/* Sortierfunktion */
void sort(int arr[], unsigned int size) {
    for (unsigned int i = 0; i < size; i++) {
        int tmp = arr[i];
        unsigned int j;
        for (j = i; j && arr[j-1] > tmp; j--)
            arr[j] = arr[j-1];
        arr[j] = tmp;
    }
}
