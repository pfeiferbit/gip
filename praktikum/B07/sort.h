/**
 * @file sort.h
 * @brief B07 - Makefile
 * @author Maximilian Pfeifer
 */

#ifndef SORT_H
#define SORT_H

/* Sortierfunktion */
void sort(int arr[], unsigned int size);

#endif
