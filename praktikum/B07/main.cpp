/**
 * @file main.cpp
 * @brief B07 - Makefile
 * @author Maximilian Pfeifer
 */

#include <iostream>

#include "sort.h"

using namespace std;

void print_out(int arr[], unsigned int size) {
    if (!size)
        cout << "Leeres Array.";
    else {
        cout << arr[0];
        for (unsigned int i = 1; i < size; i++)
            cout << ", " << arr[i];
    }
    cout << endl;
}

int main() {
    const unsigned int size = 10;
    int arr[] = {9, 3, 5, 2, 8, 6, 4, 3, 7, 8};
    cout << "Vorher: ";
    print_out(arr, size);
    sort(arr, size);
    cout << "Nachher: ";
    print_out(arr, size);
#ifdef _Win32
    system("PAUSE");
#endif
    return 0;
}
