/**
 * @file main.cpp
 * @brief Hauptprogramm A03 - MinMax
 * @author Maximilian Pfeifer
 * @date 2013-10-29
 */

#include <iostream>

using namespace std;

const int LENGTH = 5;

/**
 * @brief Einstiegspunkt
 * @return Fehlercode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    int num[LENGTH];
    int min = 0,
        max = 0;
           
    for (int i = 0; i < LENGTH; i++) {
        cout << "Bitte geben Sie die " << i+1 << ". Zahl ein: ";
        cin >> num[i];
        if (num[i] < num[min])
            min = i;
        else if (num[i] > num[max])
            max = i;
    }
           
    cout << "Die " << min+1
         << ". Zahl war die kleinste der eingegebenen Zahlen und lautet: "
         << num[min] << endl;
    cout << "Die " << max+1
         << ". Zahl war die groesste der eingegebenen Zahlen und lautet: "
         << num[max] << endl;
     
    system("PAUSE");
    return 0;
}