/**
 * @file main.cpp
 * @brief Hauptprogramm zu B03 - KryptoSchleifen
 * @author Maximilian Pfeifer
 * @date 2013-10-27
 */

#include <iostream>
#include <string>

using namespace std;

/**
 * @brief B03-01 Verschluesselung mittels for
 * @param clear Klartext
 * @param step Verschiebungsschritte
 * @return string Verschluesselte Zeichenkette
 */
string encrypt_for(string const &clear, unsigned int const &step) {
    string result;
    for (auto c = clear.begin(); c != clear.end(); ++c) {
        if (*c >= 'A' && *c <= 'Z') {
            result += (*c - 'A' + step) % 26 + 'A';
        } else if (*c >= 'a' && *c <= 'z') {
            result += (*c - 'a' + step) % 26 + 'a';
        } else {
            result += *c;
        }
    }
    return result;
}

/**
 * @brief B03-02 Verschluesselung mittels while
 * @param clear Klartext
 * @param step Verschiebungsschritte
 * @return string Verschluesselte Zeichenkette
 */
string encrypt_while(string const &clear, unsigned int const &step) {
    string result;
    auto c = clear.begin();
    while (c != clear.end()) {
        if (*c >= 'A' && *c <= 'Z') {
            result += (*c - 'A' + step) % 26 + 'A';
        } else if (*c >= 'a' && *c <= 'z') {
            result += (*c - 'a' + step) % 26 + 'a';
        } else {
            result += *c;
        }
        ++c;
    }
    return result;
}

/**
 * @brief Einstiegspunkt
 * @return Fehlercode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    string clear, crypto;
    unsigned int step;
    cout << "Bitte geben Sie den zu verschluesselnden Text ein: ";
    getline(cin, clear);
    cout << "Bitte geben Sie die Anzahl Verschiebepositionen ein (als positive ganze Zahl): ";
    cin >> step;
    crypto = encrypt_for(clear, step);
    //crypto = encrypt_while(clear, step);
    cout << crypto << endl;
    system("PAUSE");
    return 0;
}
