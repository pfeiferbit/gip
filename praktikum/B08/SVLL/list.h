/**
 * @file list.h
 * @brief B08 - Sortierte verkettete lineare Liste
 * @author Maximilian Pfeifer
 */

#ifndef LIST_H
#define LIST_H

namespace list {

    struct node {
        int data;
        node* next;
    };

    node* new_node(int data, node* next = nullptr);

    void insert(node* &root, int data);

    void printout(node* root);

}

#endif