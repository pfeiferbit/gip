/**
 * @file list.cpp
 * @brief B08 - Sortierte verkettete lineare Liste
 * @author Maximilian Pfeifer
 */

#include "list.h"

#include <iostream>

namespace list {

    node* new_node(int data, node* next) {
        node* current = new node;
        current->data = data;
        current->next = next;
        return current;
    }

    void insert(node* &root, int data) {
        if (!root) {
            root = new_node(data);
        } else if (data >= root->data) {
            root = new_node(data, root);
        } else {
            node* tmp = root;
            for (tmp; tmp->next; tmp = tmp->next)
                if (data < tmp->next->data)
                    break;
            tmp->next = new_node(data, tmp->next);
        }
    }

    void printout(node* root) {
        if (!root) {
            std::cout << "Leere Liste.";
        } else {
            for (node* tmp = root; tmp->next; tmp = tmp->next)
                std::cout << tmp->data << " ";
        }
        std::cout << std::endl;
    }

}