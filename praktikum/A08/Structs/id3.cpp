/**
 * @file id3.cpp
 * @brief A08 - Structs
 * @author Maximilian Pfeifer
 */

#include "id3.h"

#include <cstdlib>
#include <iostream>

namespace id3 {

    id3v1tag input() {
        id3v1tag new_tag;
        new_tag.label[0] = 'T';
        new_tag.label[1] = 'A';
        new_tag.label[2] = 'G';
        std::cout << "Track\t\t > ";
        std::cin >> new_tag.title;
        std::cout << "Interpret\t > ";
        std::cin >> new_tag.interpreter;
        std::cout << "Album\t\t > ";
        std::cin >> new_tag.album;
        std::cout << "Jahr\t\t > ";
        std::cin >> new_tag.year;
        std::cout << "Kommentar\t > ";
        std::cin >> new_tag.comment;
        std::cout << "Genre\t\t > ";
        std::cin >> new_tag.genre;
        return new_tag;
    }

    void search_interpreter(struct id3v1tag tracks[250],
                            char interpreter[30], int track_count) {
        for (int i = 0; i < track_count; i++) {
            if (!strcmp(interpreter, tracks[i].interpreter)) {
                std::cout << "Titel Nr. " << i+1 << std::endl
                    << tracks[i].title << std::endl
                    << tracks[i].interpreter << std::endl
                    << tracks[i].album << std::endl
                    << tracks[i].year << std::endl
                    << tracks[i].comment << std::endl
                    << genres[tracks[i].genre] << std::endl
                    << std::endl;
            }
        }
    }

}