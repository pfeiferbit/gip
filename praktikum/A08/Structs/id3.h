/**
 * @file id3.h
 * @brief A08 - Structs
 * @author Maximilian Pfeifer
 */

#ifndef ID3_H
#define ID3_H

namespace id3 {

    const char genres[][23] = {
        {"Blues"}, {"Classic Rock"}, {"Country"}, {"Dance"}, {"Disco"},
        {"Funk"}, {"Grunge"}, {"Hip-Hop"}, {"Jazz"}, {"Metal"}, {"New Age"},
        {"Oldies"},	{"Other"}, {"Pop"}, {"R&B"}, {"Rap"}, {"Reggae"}, {"Rock"},
        {"Techno"}, {"Industrial"},	{"Alternative"}, {"Ska"}, {"Death Metal"},
        {"Pranks"}, {"Soundtrack"}, {"Euro-Techno"}, {"Ambient"}, {"Trip-Hop"},
        {"Vocal"},  {"Jazz+Funk"}, {"Fusion"}, {"Trance"}, {"Classical"},
        {"Instrumental"}, {"Acid"}, {"House"}, {"Game"}, {"Sound Clip"},
        {"Gospel"}, {"Noise"}, {"Alternative Rock"}, {"Bass"}, {"Soul"},
        {"Punk"}, {"Space"}, {"Meditative"}, {"Instrumental Pop"},
        {"Instrumental Rock"}, {"Ethnic"}, {"Gothic"}, {"Darkwave"},
        {"Techno-Industrial"}, {"Electronic"}, {"Pop-Folk"}, {"Eurodance"},
        {"Dream"}, {"Southern Rock"}, {"Comedy"}, {"Cult"}, {"Gangsta"},
        {"Top 40"}, {"Christian Rap"}, {"Pop/Funk"}, {"Jungle"}, {"Native US"},
        {"Cabaret"}, {"New Wave"}, {"Psychadelic"}, {"Rave"}, {"Showtunes"},
        {"Trailer"}, {"Lo-Fi"}, {"Tribal"}, {"Acid Punk"}, {"Acid Jazz"},
        {"Polka"}, {"Retro"}, {"Musical"}, {"Rock & Roll"}, {"Hard Rock"},
        {"Folk"}, {"Folk-Rock"}, {"National Folk"}, {"Swing"}, {"Fast Fusion"},
        {"Bebob"}, {"Latin"}, {"Revival"}, {"Celtic"}, {"Bluegrass"},
        {"Avantgarde"}, {"Gothic Rock"}, {"Progressive Rock"},
        {"Psychedelic Rock"}, {"Symphonic Rock"}, {"Slow Rock"}, {"Big Band"},
        {"Chorus"}, {"Easy Listening"}, {"Acoustic"}, {"Humour"}, {"Speech"},
        {"Chanson"}, {"Opera"}, {"Chamber Music"}, {"Sonata"}, {"Symphony"},
        {"Booty Bass"}, {"Primus"}, {"Porn Groove"}, {"Satire"}, {"Slow Jam"},
        {"Club"}, {"Tango"}, {"Samba"}, {"Folklore"}, {"Ballad"},
        {"Power Ballad"}, {"Rhythmic Soul"}, {"Freestyle"}, {"Duet"},
        {"Punk Rock"}, {"Drum Solo"}, {"A Cappella"},  {"Euro-House"},
        {"Dance Hall"}, {"Goa"}, {"Drum & Bass"}, {"Club-House"}, {"Hardcore"},
        {"Terror"}, {"Indie"}, {"BritPop"}, {"Negerpunk"}, {"Polsk Punk"},
        {"Beat"}, {"Christian Gangsta Rap"}, {"Heavy Metal"}, {"Black Metal"},
        {"Crossover"}, {"Contemporary Christian"}, {"Christian Rock"},
        {"Merengue"}, {"Salsa"}, {"Thrash Metal"}, {"Anime"}, {"JPop"},
        {"Synthpop"}, {""}
    };

    struct id3v1tag {
        char label[3];
        char title[30];
        char interpreter[30];
        char album[30];
        char year[4];
        char comment[30];
        unsigned char genre;
    };

    id3v1tag input();
    void search_interpreter(struct id3v1tag tracks[250],
                            char interpreter[30], int n);

}

#endif ID3_H