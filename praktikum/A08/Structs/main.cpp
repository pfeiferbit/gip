/**
 * @file main.cpp
 * @brief A08 - Structs
 * @author Maximilian Pfeifer
 */

#include <iostream>

#include "id3.h"

using namespace std;

int main() {
    id3::id3v1tag tracks[250];
    int track_count = 0;
    while (true) {
        cout << "t - Titel eingeben" << endl
             << "i - Interpret suchen" << endl
             << "e - Programmende" << endl;
        char selection;
        cin >> selection;
        if (selection == 't') {
            cout << "Eingabe:" << endl;
            tracks[track_count] = id3::input();
            track_count++;
        }
        if (selection == 'i') {
            cout << "Interpret: ";
            char search[30];
            cin >> search;
            id3::search_interpreter(tracks, search, track_count);
        }
        if (selection == 'e')
            break;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
    }
    system("PAUSE");
    return 0;
}