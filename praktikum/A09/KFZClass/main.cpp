/**
 * @file main.cpp
 * @brief A09 - KFZ als Klasse
 * @author Maximilian Pfeifer
 */

#include <iostream>
#include <stdexcept>

using namespace std;

class Car {

private:
    char*   label;
    size_t  fillings;
    double* tank;
    double* distance;

    Car();

public:
    Car(char label[], size_t fillings) : label(label),
                                         fillings(fillings) {
        tank     = new double[fillings];
        distance = new double[fillings];
    }

    ~Car() {
        delete distance;
        delete tank;
        delete label;
    }

    void set_filling(size_t filling, double tank, double distance) {
        if (filling >= fillings)
            throw out_of_range("OOR: Maximale Fuellung ueberschritten.");
        this->tank    [filling] = tank;
        this->distance[filling] = distance;
    }

    const char* get_label() const {
        return label;
    }

    const double get_consumption_avg() const {
        double sum_tank     = 0,
               sum_distance = 0;
        for (size_t i = 0; i < fillings; i++) {
            sum_tank     += tank[i];
            sum_distance += distance[i];
        }
        return sum_tank / sum_distance * double(100);
    }

};

int main() {
    const size_t fillings = 3;
    char car_label[30];
    cout << "Kennzeichen: ";
    cin >> car_label;
    Car car(car_label, fillings);
    cout << endl;
    for (size_t i = 0; i < fillings; i++) {
        double tank, distance;
        cout << "Tankfuellung " << i+1 << " [l]  : ";
        cin >> tank;
        cout << "Wegstrecke   " << i+1 << " [km] : ";
        cin >> distance;
        car.set_filling(i, tank, distance);
    }
    cout << endl << car.get_label() << ":" << endl
         << "Mittlerer Verbrauch [l / 100 km] : " << car.get_consumption_avg()
         << endl << endl;
    system("PAUSE");
    return 0;
}