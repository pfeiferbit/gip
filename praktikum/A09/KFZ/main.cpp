/**
 * @file main.cpp
 * @brief A09 - KFZ ohne Klassen
 * @author Maximilian Pfeifer
 */

#include <iostream>

using namespace std;

const int fillings = 3;

double avg(double tank[], double distance[]) {
    double sum_tank     = 0,
           sum_distance = 0;
    for (int i = 0; i < fillings; i++) {
        sum_tank     += tank[i];
        sum_distance += distance[i];
    }
    return sum_tank / sum_distance * double(100);
}

int main() {
    char car[30];
    cout << "Kennzeichen: ";
    cin >> car;
    cout << endl;
    double tank[fillings];
    double distance[fillings];
    for (int i = 0; i < fillings; i++) {
        cout << "Tankfuellung " << i+1 << " [l]  : ";
        cin >> tank[i];
        cout << "Wegstrecke   " << i+1 << " [km] : ";
        cin >> distance[i];
    }
    cout << endl << car << ":" << endl
         << "Mittlerer Verbrauch [l / 100 km] : " << avg(tank, distance)
         << endl << endl;
    system("PAUSE");
    return 0;
}