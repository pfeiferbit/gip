/**
 * @file bst.h
 * @brief A07 - Duplikatfreier binaerer Suchbaum
 * @author Maximilian Pfeifer
 */

#ifndef BST_H
#define BST_H

#include <iostream>
#include <string>

namespace bst {

    template <typename T>
    struct node {
        T data;
        node<T>* left;
        node<T>* right;
    };

    template <typename T>
    void insert(node<T>* &root, T data) {
        node<T>* current = new node<T>;
        current->data = data;
        current->left = nullptr;
        current->right = nullptr;
        if (!root) {
            root = current;
        } else {
            node<T>** tmp = &root;
            while  (*tmp) {
                if (data == (*tmp)->data)
                    return;
                tmp = data < (*tmp)->data ? &(*tmp)->left : &(*tmp)->right;
            }
            *tmp = current;
        }
    }

    template <typename T>
    void printout(node<T>* root) {
        if (!root)
            std::cout << "Leerer Baum." << std::endl;
        else {
            if (root->right)
                printout(root->right, 1);
            std::cout << root->data << std::endl;
            if (root->left)
                printout(root->left, 1);
        }
    }

    template <typename T>
    void printout(node<T>* current, int depth) {
        if (current->right)
            printout(current->right, depth+1);
        std::cout << std::string(depth*3, '.') << current->data << std::endl;
        if (current->left)
            printout(current->left, depth+1);
    }

}

#endif