/**
 * @file main.cpp
 * @brief A07 - Duplikatfreier binaerer Suchbaum
 * @author Maximilian Pfeifer
 */

#include <iostream>

#include "bst.h"

using namespace std;

int main() {
    bst::node<int>* root = nullptr;
    bst::printout(root);
    int input;
    while (true) {
        cout << "Naechster Wert (99 beendet)? ";
        cin >> input;
        if (input == 99)
            break;
        bst::insert(root, input);
    }
    bst::printout(root);
    system("PAUSE");
    return 0;
}