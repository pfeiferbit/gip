/**
 * @file   main.cpp
 * @brief  A12 - Dateioperationen
 * @author Maximilian Pfeifer
 */

#include <string>

#include <iostream>
#include <fstream>

using namespace std;

bool upper(ifstream &in, ofstream &out) {
    if (!in || !out)
        return false;
    else {
        while (!in.eof()) {
            char c = in.get();
            if (c >= 'a' && c <= 'z') {
                c -= 'a' - 'A';
            }
            if (c != -1) {
                out.put(c);
            }
        }
        return true;
    }
}

int main() {
    char fn_in[30],
         fn_out[30];
    cout << "Einlesen: Dateiname = ? ";
    cin >> fn_in;
    cout << "Ausgeben: Dateiname = ? ";
    cin >> fn_out;
    ifstream f_in;
    f_in.open(fn_in, ios::in | ios::binary);
    ofstream f_out;
    f_out.open(fn_out, ios::out | ios::binary);
    if (!upper(f_in, f_out)) {
        cout << "Fehler.";
    }
    f_in.close();
    f_out.close();
    cout << endl;
    system("PAUSE");
    return 0;
}