/**
 * @file calc.cpp
 * @brief B05 - Kalkulationsmodul (Implementierung)
 * @author Maximilian Pfeifer
 * @date 2013-11-09
 */

#include "calc.h"

namespace calc {

    /**
     * @brief Ermittelt die Summe oder Differenz zweier Operanden
     * @param left Linker Operand
     * @param right Rechter Operand
     * @param operation Definiert Operation ('+' | '-')
     * @return int Ergebnis
     */
    const int calculate (int const &left, int const &right,
                         char const &operation) {
        if (operation == '+')
            return left + right;
        else if (operation == '-')
            return left - right;
        else
            return 0;
    }

}
