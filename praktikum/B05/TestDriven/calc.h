/**
 * @file calc.h
 * @brief B05 - Kalkulationsmodul (Header)
 * @author Maximilian Pfeifer
 * @date 2013-11-09
 */

#ifndef CALC_H
#define CALC_H

namespace calc {

    /**
     * @brief Ermittelt die Summe oder Differenz zweier Operanden
     * @param left Linker Operand
     * @param right Rechter Operand
     * @param operation Definiert Operation ('+' | '-')
     * @return int Ergebnis
     */
    const int calculate(int const &left, int const &right,
                        char const &operation);

}

#endif
