/**
 * @file gui.h
 * @brief B05 - Ausgabemodul (Header)
 * @author Maximilian Pfeifer
 * @date 2013-11-09
 */

#ifndef GUI_H
#define GUI_H

namespace gui {

    /**
     * @brief Gibt das Ergebnis aus
     * @input result Ergebniswert
     */
    void printout(const int &result);

    /**
     * @brief Nimmt Eingaben entgegen und ueberprueft diese auf Korrektheit
     * @param digits Array der einzelnen Ziffern
     * @param operation Anzuwendende Operation
     */
    void input(char (&digits)[2][2], char &operation);

}

#endif
