/**
 * @file main.cpp
 * @brief B05 - Hauptprogramm
 * @author Maximilian Pfeifer
 * @date 2013-11-09
 */

#include <iostream>
#include <cassert>

#include "calc.h"
#include "gui.h"

/**
 * @brief Testet calc::calculate()
 */
void test_calculate() {
    for (int left = 0; left <= 99; left++) {
        for (int right = 0; right <= 99; right++) {
            assert(calc::calculate(left, right, '+') == left + right);
            assert(calc::calculate(left, right, '-') == left - right);
        }
    }
}

/**
 * @brief Einstiegspunkt
 * @return int Fehlercode
 */
int main() {
    //test_calculate();
    char digits[2][2] = {0}; // Warum Array? --> siehe gui::input()
    char operation = 0;
    gui::input(digits, operation);
    int left  = (int)(digits[0][0] - '0') * 10 + (int)(digits[0][1] - '0');
    int right = (int)(digits[1][0] - '0') * 10 + (int)(digits[1][1] - '0');
    gui::printout(calc::calculate(left, right, operation));
    system("PAUSE");
    return 0;
}
