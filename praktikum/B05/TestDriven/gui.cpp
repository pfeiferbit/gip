/**
 * @file gui.cpp
 * @brief B05 - Ausgabemodul (Implementierung)
 * @author Maximilian Pfeifer
 * @date 2013-11-09
 */

#include "gui.h"

#include <iostream>

#include "calc.h"

namespace gui {

    /**
     * @brief Gibt das Ergebnis aus
     * @input result Ergebniswert
     */
    void printout(const int &result) {
        std::cout << "Das Ergebnis lautet: " << result << std::endl;
    }

    /**
     * @brief Nimmt Eingaben entgegen und ueberprueft diese auf Korrektheit
     * @param digits Array der einzelnen Ziffern
     * @param operation Anzuwendende Operation
     */
    void input(char (&digits)[2][2], char &operation) {
        // Array erspart Wiederholungen des while-Konstrukts, da durch 
        // ein Array iteriert werden kann.
        // Aeusseres Array (i) repraesentiert einzelne Operanden.
        // Inneres Array (j) repraesentiert einzelne Ziffern eines Operanden.
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                while (!(digits[i][j] >= '0' && digits[i][j] <= '9')) {
                    std::cout << "Geben Sie die " << j+1
                        << ". Ziffer der " << i+1 << ". Zahl ein: ";
                    std::cin >> digits[i][j];
                }
            }
        }
        // Operation extra, da das while-Konstrukt abweicht.
        while (!(operation == '+' || operation == '-')) {
            std::cout << "Geben Sie die Rechenoperation ein (+ pder -): ";
            std::cin >> operation;
        }
    }

}
