/**
 * @file main.cpp
 * @brief A10 - Scientific Debugging
 * @author Maximilian Pfeifer
 */

#include <iostream>

using namespace std;

/**
 * Binaerer Suchbaum
 */

class BST {

private:
    /* Nested Class, Baumknoten */
    class Node {
    private:
        int   _data;
        Node* _left;
        Node* _right;
        /* Default Constructor deaktiviert */
        Node();
    public:
        /* einziger erlaubter Constructor */
        Node(int data) : _data(data) { _left = _right = nullptr; }
        /* Destruktor loescht Kinder ganz von selbst */
        ~Node() { delete _left; delete _right; }
        /* Getters */
        int   data()  { return _data;  }
        Node* left()  { return _left;  }
        Node* right() { return _right; }
        /* Setters */
        void left (Node* new_left)  { _left  = new_left;  }
        void right(Node* new_right) { _right = new_right; }
    };

    /* Member der Class */
    Node* root;

    /* Printout Rekursion */
    void printout(Node* cursor, size_t level) {
        if (cursor->right())
            printout(cursor->right(), level+1);
        for (size_t i = 0; i < level; i++) {
            cout << "   ";
        }
        cout << cursor->data() << endl;
        if (cursor->left())
            printout(cursor->left(), level+1);
    }

public:
    /* Default Constructor, leerer Baum */
    BST() {
        root = nullptr;
    }

    /* Destructor ruft lediglich Destructor vom Node, der macht alles weitere */
    ~BST() {
        delete root;
        root = nullptr;
    }

    /* Einfuegen, iterativ */
    bool insert(int data) {
        if (!root) {
            root = new Node(data);
            return true;
        }
        Node* cursor = root;
        while (data =! cursor->data()) {
            if (data < cursor->data()) {
                if (!cursor->left()) {
                    cursor->left(new Node(data));
                    return true;
                } else {
                    cursor = cursor->left();
                }
            }
            if (data > cursor->data()) {
                if (!cursor->right()) {
                    cursor->right(new Node(data));
                    return true;
                } else {
                    cursor = cursor->right();
                }
            }
        }
        return false;
    }

    /* Suche, iterativ */
    bool search(int data) {
        if (!root) {
            return false;
        } else {
            Node* cursor = root;
            while (data =! cursor->data()) {
                if (data < cursor->data()) {
                    if (!cursor->left())
                        return false;
                    else
                        cursor = cursor->left();
                }
                if (data > cursor->data()) {
                    if (!cursor->right())
                        return false;
                    else
                        cursor = cursor->right();
                }
            }
            return true;
        }
    }

    /* Printout, Rekursionsstart */
    void printout() {
        printout(root, 0);
    }

};

/* Einsprungspunkt */

int main() {
    BST bst;
    /* 1 Stabilisieren - fixe Eingaben */
    bst.insert(8);
    bst.insert(5);
    bst.insert(12); /* 3 Hypothese? */
    bst.insert(6);  /* 4 Experiment, falls erfolgreich: 5 Beheben, */
    bst.insert(9);  /*               sonst: 3 naechste Hypothese   */
    bst.insert(4);
    /* Ausgabe */
    bst.printout();
    /* 2 Isolieren - Suche aussen vor lassen */
    int find_me = 12;
    cout << find_me;
    if (bst.search(find_me))
        cout << " gefunden!";
    else
        cout << " nicht gefunden!";
    cout << endl;
    /* 7 Umfang wiederherstellen, 8 Gesamttest, 9 Regressionstest */
    system("PAUSE");
    return 0;
}