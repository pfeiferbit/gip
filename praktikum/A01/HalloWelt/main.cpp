/**
 * @file main.cpp
 * @brief GIP Praktikum A01: HalloWelt
 * @author Maximilian Pfeifer
 * @date 2013-10-08
 */

#include <iostream>

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
	std::cout << "Hallo Welt" << std::endl;
	system("PAUSE");
	return 0;

	// A01.03
	// Wenn die folgenden beiden Zeilen in einem Blockkommentar stehen,
	// werden sie nicht ausgefuehrt. Obwohl 0 nicht explizit als Wert
	// zurueckgegeben wird, geschieht es implizit:
	// Das Programm "..." wurde mit Code 0 (0x0) beendet.
	// return 0 repr�sentiert den Errorcode 0 beim beenden.
	
	// A01.04
	// Wenn alles auskommentiert wird, wird nichts davon ausgefuehrt.
	// Das Programm kompiliert dennoch erfolgreich, und wird erfolgreich
	// ausgefuehrt, obwohl dabei fuer den Endnutzer nichts geschieht.
	// (Zu Errorcode siehe A01.03)

	// A01.06
	// Einzelne Anweisungen k�nnen auf mehreren Zeilen geschrieben werden.

	// A01.07
	// Wenn z.B. Semikolons fehlen berichtet der Compiler einen Syntaxfehler:
	// 1>ClCompile:
	// 1>  main.cpp
	// 1>h:\...\main.cpp(23): error C2143: Syntaxfehler: Es fehlt ';' vor '}'
	//
	// Bei mir funktioniert IntelliSense aber nicht :(
	
	// A01.08
	// Mehrere Anweisungen k�nnen auf einer Zeile geschrieben werden.

	// A01.09
	// Anweisungsbl�cke �ndern HIER nichts an der Ausf�hrung.
	// Jedoch sind Variablen, die innerhalb eines Blocks deklariert
	// (und initialisiert) werden, von au�erhalb nicht ansprechbar.
}