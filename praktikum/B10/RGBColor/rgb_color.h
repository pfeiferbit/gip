/**
 * @file rgb_color.h
 * @brief B10 - Objektorientierung - RGB_Color
 * @author Maximilian Pfeifer
 */

#ifndef RGB_COLOR_H
#define RGB_COLOR_H

class RGB_Color {

private:
    unsigned char red;
    unsigned char green;
    unsigned char blue;
    int inputNumber();

public:
    RGB_Color();
    RGB_Color(unsigned char red, unsigned char green,
              unsigned char blue);
    void setColor(unsigned char red, unsigned char green,
                  unsigned char blue);
    unsigned char getR() { return red; }
    unsigned char getG() { return green; }
    unsigned char getB() { return blue; }
    void display();
    bool chooseColor();

};

#endif