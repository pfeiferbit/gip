/**
 * @file rgb_color.cpp
 * @brief B10 - Objektorientierung - RGB_Color
 * @author Maximilian Pfeifer
 */

#include "rgb_color.h"

#include <string>
#include <iostream>
#include <sstream>

/* Default constructor, white */
RGB_Color::RGB_Color() {
    red = green = blue = 255;
}

/* Custom constructor, initializing color with given values */
RGB_Color::RGB_Color(unsigned char red, unsigned char green,
                     unsigned char blue)
        : red(red), green(green), blue(blue) {}

/* Sets color values */
void RGB_Color::setColor(unsigned char red, unsigned char green,
                         unsigned char blue) {
    this->red   = red;
    this->green = green;
    this->blue  = blue;
}

/* Prints color values on screen */
void RGB_Color::display() {
    std::cout << "( R = "
              << (red   > 100 ? "" : " ") << (red   > 10 ? "" : " ")
              << int(red) << " , G = "
              << (green > 100 ? "" : " ") << (green > 10 ? "" : " ")
              << int(green) << " , B = "
              << (blue  > 100 ? "" : " ") << (blue  > 10 ? "" : " ")
              << int(blue) << " )";
}

/* Takes an input string and converts it to a number (int) */
int RGB_Color::inputNumber() {
    std::string input;
    std::getline(std::cin, input);
    std::istringstream instream(input);
    int number;
    instream >> number;
    return number;
}

/* Takes color values from input, only sets any of them if all are valid */
bool RGB_Color::chooseColor() {
    std::cout << "rot   [0:255] = ";
    int in_red = inputNumber();
    if (in_red   < 0 || in_red   > 255) return false;
    std::cout << "gruen [0:255] = ";
    int in_green = inputNumber();
    if (in_green < 0 || in_green > 255) return false;
    std::cout << "blau  [0:255] = ";
    int in_blue = inputNumber();
    if (in_blue  < 0 || in_blue  > 255) return false;
    red   = unsigned char(in_red);
    green = unsigned char(in_green);
    blue  = unsigned char(in_blue);
    return true;
}