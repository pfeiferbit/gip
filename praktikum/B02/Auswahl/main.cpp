/**
 * @file main.cpp
 * @brief GIP Praktikum B02: Umrechnung - Auswahl
 * @author Maximilian Pfeifer
 * @date 2013-10-17
 */

#include <iostream>
using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    double in,  // Eingabe
           out;	// Ausgabe
    int    sel; // Auswahl

    cout << "Ihre Eingabe: ";
    cin >> in;
    cout << "Ihre Auswahl der Umwandlung:" << endl
         << " 1 - Celsius in Fahrenheit" << endl
         << " 2 - Meter in Fuss" << endl
         << " 3 - Euro in US-Dollar" << endl;
    cin >> sel;

    /*
    tempF     = tempC     * 1.8 + 32;
    lengthF   = lengthM   * 3.2808; 
    currencyD = currencyE * 1.2957;
    */

    out =
        // bei sel=1 -> x * (-1) * (-2) * 0.5 = x
        (in*1.8+32) * (sel-2) * (sel-3) * 0.5 +

        // bei sel=2 -> x * ( 1) * (-1) * (-1) = x
        (in*3.2808) * (sel-1) * (sel-3) * (-1) +

        // bei sel=3 -> x * ( 2) * ( 1) * 0.5 = x
        (in*1.2957) * (sel-1) * (sel-2) * 0.5;

    cout << "Ihr Ergebnis lautet: " << out << endl;

    system ("PAUSE");
    return 0;
}