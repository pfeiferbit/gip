/**
 * @file main.cpp
 * @brief GIP Praktikum B02: Umrechnung - CelsiusFahrenheit
 * @author Maximilian Pfeifer
 * @date 2013-10-17
 */

#include <iostream>
using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    double tempC, tempF;

    cout << "Bitte geben Sie die Temperatur in Grad Celsius ein: ";
    cin >> tempC;

    tempF = tempC * 1.8 + 32;

    cout << "Die Temperatur in Grad Fahrenheit lautet: " << tempF << endl;

    system ("PAUSE");
    return 0;
}