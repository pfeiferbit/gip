/**
 * @file main.cpp
 * @brief GIP Praktikum B02: Umrechnung - EuroDollar
 * @author Maximilian Pfeifer
 * @date 2013-10-17
 */

#include <iostream>
using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    double currencyE, currencyD;

    cout << "Bitte geben Sie die Geldmenge in Euro ein: ";
    cin >> currencyE;

    currencyD = currencyE * 1.2957;

    cout << "Die Geldmenge in US-Dollar lautet: " << currencyD << endl;

    system ("PAUSE");
    return 0;
}