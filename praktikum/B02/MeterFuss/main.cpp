/**
 * @file main.cpp
 * @brief GIP Praktikum B02: Umrechnung - MeterFuss
 * @author Maximilian Pfeifer
 * @date 2013-10-17
 */

#include <iostream>
using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    double lengthM, lengthF;

    cout << "Bitte geben Sie die Laenge in Metern ein: ";
    cin >> lengthM;

    lengthF = lengthM * 3.2808;

    cout << "Die Laenge in Fuss lautet: " << lengthF << endl;

    system ("PAUSE");
    return 0;
}