/**
 * @file main.cpp
 * @brief GIP Praktikum B01: HalloWelt
 * @author Maximilian Pfeifer
 * @date 2013-10-10
 */

#include <iostream>

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {

	/**
	 * B01.06
	 * Wenn man den Name main umbenennt, findet der Linker keinen Einstiegspunkt.
	 * und kann die Verlinkung nicht durchf�hren. Entsprechend kann dann auch
	 * nicht kompiliert werden.
	 */

	/**
	 * B01.07
	 * R�ckgabetyp muss definiert werden. Int wird zwar zuerst angenommen,
	 * aber von C++ nicht unterst�tzt.
	 */

	/**
	 * B01.08
	 * Der Compiler meckert dann �ber nichtdeklarierte Bezeichner.
	 * Folglich wird das Programm nicht kompiliert.
	 */

	// B01.03

	// Hier hinter kann man alles schreiben. // <-- wird gar nicht beachtet

	/*
		Zeilenkommentar
		// in Blockkommentar
		funktioniert auch, selber Grund
	*/

	/*
		Das hier funktioniert nicht,
		/* <-- weil das hier nicht beachtet wird
		aber
		*/ //<-- das hier schon und somit der Kommentar beendet ist.

	// Blockkommentar: /* funktioniert, weil wie bei (1) nichts beachtet wird. */

	// B01.04
	// Blockkommentare dienen dazu, innerhalb einer Zeile etwas,
	// und nicht gleich den gesamten Rest der Zeile auszukommentieren.

	/*<-- blubb
	std::cout /* << "Hallo Welt" << */ std::endl;
	// */

	// B01.05
	// kompiliert nicht, weil der bei blubb begonnene Blockkommentar
	// schon bei "Hallo Welt" beendet wird. */ ist keine legitime Syntax.

	system ("PAUSE");
	return 0;
}