/**
 * @file main.cpp
 * @brief GIP Praktikum B01: VarInput
 * @author Maximilian Pfeifer
 * @date 2013-10-10
 */

#include <iostream>

using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    char input;
    cout << "Bitte geben Sie Ihre Studienrichtung ein:" << endl
         << "1: Informatik" << endl
         << "2: Elektrotechnik" << endl;
    cin >> input;
    cout << "Ihre Eingabe lautete: " << input << endl;
    system("PAUSE");
    return 0;
}