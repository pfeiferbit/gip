/**
 * @file main.cpp
 * @brief GIP Praktikum B01: Limits
 * @author Maximilian Pfeifer
 * @date 2013-10-10
 */

#include <iostream>
#include <limits>

using namespace std;

/**
 * @brief Einstiegspunkt
 * @return int Errorcode 0 bei erfolgreicher Ausfuehrung
 */
int main() {
    cout << "Die groesste speicherbare Integerzahl lautet: "
         << INT_MAX << endl
         << "Die kleinste speicherbare Integerzahl lautet: "
         << INT_MIN << endl;
    system("PAUSE");
    return 0;
}